function oData_buildProperty(obj, name) {
	Object.defineProperty(obj, name, {
       get: function() { return this.get(name); },
       set: function(value) {return this.set(name, value);}
    });
}

function oData_buildReadOnlyProperty(obj, name) {
	Object.defineProperty(obj, name, {
       get: function() { return this.get(name); }
    });
}


//--------------------------------------------------------------------------------
// objectPrototipe
//--------------------------------------------------------------------------------
function oData_objectPrototype(data,factory){
	this.$data={};
	this.$special={};
	this.$navigation={};
	this.$changes={};
	this.$hasChanges=false;
	this.$factory=factory;
	
	this.$references_list=[];
	this.$references_object=[];
	
	for(key in data){
		var value=data[key];
		
		if (key.search("@odata.type")!=-1)
			continue;
		
		if (factory.ignoreFields.indexOf(key)==-1){
			if (factory.specialFields.indexOf(key)!=-1){
				this.$special[key]=value;
			}
			else{
				if (key.search("odata.navigationLinkUrl")!=-1){
					this.$navigation[key.split("@")[0]]=value;
				}
				else{
					if (value instanceof Array){
							var list=[];
							this.$data[key]=list;
							this.$references_list.push(key);
							oData_buildReadOnlyProperty(this,key);	
							
							for(k in value)
								list.push(new oData_objectPrototype(value[k],this.$factory));
							
							
					}else
					if (value instanceof Object){
						this.$references_object.push(key);
						oData_buildReadOnlyProperty(this,key);	
						
						this.$data[key]=new oData_objectPrototype(value,this.$factory);
					}
					else{
						oData_buildProperty(this,key);
						
						this.$data[key]=value;
						this.$changes[key]=false;
					}
				}
			}
		}
	}
	
	Object.defineProperty(this, "Id", {
       get: function() { return this.$special.Id; }
    });
    
	Object.defineProperty(this, "$id", {
       get: function() { return this.$special["odata.editLink"]; }
    });
	
	this.$factory.registry_add(this);
	
	/**
	Attribute getter	
	*/
	this.get=function(key){
		return this.$data[key];
	}
	
	/**
	Attribute setter
	*/
	this.set=function(key,value){
		this.$data[key]=value;
		this.$changes[key]=true;
		this.$hasChanges=true;
	}
	
	/**
	Print this object in console	
	*/
	this.print=function(){
		console.info(this.$data);
	}
		
	/**
	Update this object
	*/
	this.update=function(config){
		config = config || {};
		config.success = config.success || function(data){};
		config.error = config.error || function(err){console.debug("Error",err)};
		
		if (this.$hasChanges){
			
			var data={};
			for(i in this.$changes){
				if (this.$changes[i])
					data[i]=this.$data[i];
			}
			
			var req = {
				method: 'MERGE',
				url: this.$special["odata.id"],
				headers: {
					"Content-Type": "application/json", 
					Accept: "application/json;odata=fullmetadata",
					"If-Match" : this.$special["odata.etag"]
				},
				data: data,
			}
					
			this.$factory.$http(req).success(config.success).error(config.error);
		}
		
		for (k in this.$references_list)
			for (k2 in this.$data[this.$references_list[k]])
				this.$data[this.$references_list[k]][k2].update();
				
		for (k in this.$references_object)
			this.$data[this.$references_object[k]].update();
	}
	
	/**
	Delete this object
	*/
	this.delete=function(config){
		config = config || {};
		config.success = config.success || function(data){};
		config.error = config.error || function(err){console.debug("Error",err)};
		
		var req = {
				method: 'DELETE',
			url: this.$special["odata.id"],
			headers: {
				"Content-Type": "application/json", 
				Accept: "application/json;odata=fullmetadata",
				"If-Match" : this.$special["odata.etag"]
			},
			data: data,
		}
				
		this.$factory.$http(req).success(config.success).error(config.error);
	}
};



//--------------------------------------------------------------------------------
// Factory
//--------------------------------------------------------------------------------
function oData_factory($http,ServiceRoot){
	this.ignoreFields=["$$hashKey", "Created@odata.type", "CreatedBy", "Modified@odata.type", "ModifiedBy", "RowVersion", "RowVersion@odata.type"];
	this.specialFields=["Created", "Modified", "odata.etag", "odata.id", "odata.type","Id","odata.editLink"];
	
	this._registry={}
	
	this.ServiceRoot=ServiceRoot;
	this.$http=$http;

	/**
	(internal) Register a new object
	*/
	this.registry_add=function(object){
		this._registry[object.$id]=object;
	}
	
	/**
	Get registered object by name and id
	*/
	this.registry_get=function(name,id){
		var link=name+"("+id+")";
		return this._registry[link];
	}
	
	/**
	Get object by link (previus registered)
	*/
	this.registry_getByLink=function(link){
		return this._registry[link];
	}
	
		
	/**
	(internal) Convert query object to query string 	
	*/
	this._prepareQueryFromOptions=function(options){
		var query="";
		if (options!=undefined){
			query="?";
			for (var key in options) 
				query=query+"$"+key+"="+options[key]+"&";
		}
				
		return query;
	}
	
	/**
	(internal) Prepare request with method, url and data
	*/
	this._prepareRequest=function(url,method,data){
		data = data || null;

		return {
			method: method,
			url: url,
			headers: {
				"Content-Type": "application/json", 
				Accept: "application/json;odata=fullmetadata"
			},
			data: data,
			__factory:this
		}
	}
	
	/**
	(internal) execute operation	
	*/
	this._op=function(url,method,successCallback,errorCallback,dataIn){
		var req=this._prepareRequest(url,method,dataIn);
		
		this.$http(req).success(function(data,status,headers,config){
			
			if (data.value!=undefined)
		    	data=data.value;
		   
			if (data instanceof Array){
				result=[];
				for(i in data)
					result.push(new oData_objectPrototype(data[i],config.__factory));
				
				successCallback(result);
			}
			else{
				successCallback(new oData_objectPrototype(data,config.__factory));	
			}
			
		}).error(function(data, status, headers, config){
			if (errorCallback!==undefined)
				errorCallback(data,status);
		});
	}
	
	/**
	Get one object	
	*/
	this.get=function(ObjectName,id,config){
		config = config || {}
		this._op(this.ServiceRoot+ObjectName+"("+id+")"+this._prepareQueryFromOptions(config.query), "GET", config.success, config.error);
	}
	
	/**
	Get multiple elements	
	*/
	this.query=function(ObjectName,config){
		config = config || {}
		this._op(this.ServiceRoot+ObjectName+this._prepareQueryFromOptions(config.query), "GET", config.success, config.error);	
	}
	
	/**
	Create a new object
	*/
	this.create=function(ObjectName,config){
		this._op(this.ServiceRoot+ObjectName+this._prepareQueryFromOptions(config.query), "POST", config.success, config.error,config.data);	
	}
	
	/**
	Update all elements in registry
	*/
	this.update=function(){
		for (k in this._registry)
			this._registry[k].update();
	}
}


